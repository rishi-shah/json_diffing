/*
 ============================================================================
 Name        : json_diff.cpp
 Author      : 
 Version     :
 ============================================================================
 */

#include <iostream>
#include <iomanip>
#include <nlohmann/json.hpp>
#include <fstream>
#include <string>

using json = nlohmann::json;

int main()
{
	//    the source document

    std::ifstream i("showAllFullyPopulated.json");
    json source;

    //     the target document

    std::ifstream j("showAllFullyPopulated2.json");
    json target;

    i >> source;
    j >> target;


    // create the patch
    json patch = json::diff(source, target);

    int iasd = 1;
    std::string str2 = std::to_string(iasd);
    std::string str3 = "/path\"";
    std::string str1 = "\"/";

    std::string newpath = str1 + str2 +str3;

    std::cout << newpath << std::endl;
    // output patch and round-trip result
   // std::string nsd = "_json_pointer";
    std::cout << std::setw(4) << patch["/1/path"_json_pointer] << std::endl;
    std::cout << std::setw(4) << patch << std::endl;
    //std::cout << nsd << std::endl;

}

